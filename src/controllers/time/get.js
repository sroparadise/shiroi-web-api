import moment from 'moment';

const handler = async ({
    db,
}) => {
    const [{ server_time }] = await db.raw('select CURRENT_TIMESTAMP as server_time');

    return {
        statusCode: 200,
        data: {
            timestamp: moment(server_time).utc(),
        },
    };
};

export default handler;