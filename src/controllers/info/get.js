const handler = async ({
    db,
}) => {
    const data = await db.raw('exec WEB_GetQuestInfo');

    return {
        statusCode: 200,
        data,
    };
};
export default handler;
