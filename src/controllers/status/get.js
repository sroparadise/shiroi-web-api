import config from '@lib/config';

const handler = async ({
    db,
}) => {
    const [{ nUserCount }] = await db.raw('exec WEB_GetOnlinePlayers');

    return {
        statusCode: 200,
        data: {
            count: nUserCount,
            capacity: parseInt(config.GAME.capacity),
        },
    };
};
export default handler;
