import md5 from 'md5';

const error = message => ({
    statusCode: 400,
    data: {
        message,
    },
});

const handler = async ({
    user: {
        sub,
        reset,
    },
    body: {
        password,
        newPassword,
    },
    db,
}) => {
    if (newPassword.length < 6) return error('invalid_new_password');

    let user;

    try {
        if (reset) {
            const [_user] = await db.table('TB_User').where({
                JID: sub,
            }).select();
            user = _user;
        } else {
            const [_user] = await db.table('TB_User').where({
                JID: sub,
                password: md5(password),
            }).select();
            user = _user;
        }
       
        if (!user) return error('invalid_password');
        
        await db.table('TB_User').where({ JID: sub }).update({ password: md5(newPassword) });
        return {
            statusCode: 200,
            data: {
                message: 'password_changed',
            },
        };
    } catch (e) {
        return error('unknown_error');
    }
};

export default handler;
