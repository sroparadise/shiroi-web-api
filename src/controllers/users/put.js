import emailValidator from 'email-validator';
import config from '@lib/config';
import { transporter } from '@lib/mailer';
import html from '@lib/templates/password';
import jwt from '@lib/jwt';

const error = message => ({
    statusCode: 400,
    data: {
        message,
    },
});

const handler = async ({
    user,
    body: {
        email,
    },
    db,
}, req) => {
    if (user.sub) return {
        statusCode: 405,
        data: {
            message: 'user_logged_in',
        },
    };

    // Validation:
    if (!emailValidator.validate(email)) return error('email_invalid');

    try {
        const [user] = await db.table('TB_User').where({
            Email: email,
        }).select();
    
        if (!user) return error('email_not_found');

        const token = jwt.create({
            iat: new Date(),
            sub: user.JID,
            reset: true,
        }, 30);

        await transporter.sendMail({
            from: `"Shiroi Online" <${config.SMTP.auth.user}>`,
            to: email,
            subject: `Shiroi Account Recovery`,
            html: html({ username: user.StrUserID, token }),
        });

        return {
            statusCode: 200,
            data: {
                message: `reset_email_sent`,
            },
        };
    } catch (e) {
        return error('failed_to_create');
    }
};

export default handler;
