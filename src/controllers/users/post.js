import emailValidator from 'email-validator';
import config from '@lib/config';
import { transporter } from '@lib/mailer';
import html from '@lib/templates/verification';
import md5 from 'md5';
import jwt from '@lib/jwt';
import proxyCheck from '@lib/proxyCheck';

const error = message => ({
    statusCode: 400,
    data: {
        message,
    },
});

const handler = async ({
    user,
    body: {
        username,
        email,
        password,
        agreement,
    },
    db,
}, req) => {
    if (user.sub) return {
        statusCode: 405,
        data: {
            message: 'user_logged_in',
        },
    };

    const ip = req.get('x-forwarded-for') || req.ip;

    const ipdetect = await proxyCheck(ip);

    if (!ipdetect) return error('bad_ipaddr');

    username = username.toLowerCase();
    email = email.toLowerCase();

    // Validation:
    if (!emailValidator.validate(email)) return error('email_invalid');

    const [userExists, emailExists] = await Promise.all([
        db.table('TB_User').where({
            StrUserID: username,
        }).select(),
        db.table('TB_User').where({
            Email: email,
        }).select(),
    ]);

    if (userExists.length) return error('username_unavailable');

    if (emailExists.length) return error('email_unavailable');

    if (!agreement) return error('invalid_agreement');

    if (password.length < 6) return error('password_invalid');

    if (username.length < 3 || !username.match(/^[a-z0-9_]+$/i)) return error('username_invalid');
    
    try {
        // Create user:
        await db.table('TB_User').insert({
            StrUserID: username,
            password: md5(password),
            Email: email,
            EmailValidate: 1,
            sec_primary: 3,
            sec_content: 3,
            reg_ip: ip,
        });

        // Get user:
        const [user] = await db.table('TB_User').where({
            StrUserID: username,
            Email: email,
        }).select();

        // Generate token valid 30 days:
        const token = jwt.create({
            iat: new Date(),
            sub: user.JID,
            verify: true,
        }, 30);

        // Send verification:
        // await transporter.sendMail({
        //     from: `"Shiroi Online" <${config.SMTP.auth.user}>`,
        //     to: email,
        //     subject: `Shiroi Account Activation`,
        //     html: html({ username, token }),
        // });

        return {
            statusCode: 200,
            data: {
                message: `email_verification_required`,
            },
        };
    } catch (e) {
        return error('failed_to_create');
    }
};

export default handler;
