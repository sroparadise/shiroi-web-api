const handler = async ({
    db,
}, req) => {
    let data;

    const type = req.query.type || 'players';

    switch (type) {
        case 'players':
            data = await db.raw('exec WEB_GetPlayerRanking 100');
        break;
        case 'job':
            data = await db.raw('exec WEB_GetJobRanking 100');
        break;
        default:
            return {
                statusCode: 400,
                data: {
                    message: 'invalid_request',
                },
            };
    }
    
    return {
        statusCode: 200,
        data,
    };
};
export default handler;
