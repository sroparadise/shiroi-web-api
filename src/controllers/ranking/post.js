const handler = async({
    body: {
        charname,
    },
    db,
}) => {
    const data = await db.raw('exec WEB_GetCharEquipment ?', [charname]);

    return {
        statusCode: 200,
        data: data.reduce((obj, {
            slot,
            name_key,
            icon,
            plus_value,
            rarity,
            set_item,
            variance,
        }) => {
            return {
                ...obj,
                [slot]: {
                    key: name_key,
                    icon,
                    plus_value,
                    rarity,
                    set_item,
                    variance,
                },
            };
        }, {}),
    };
};
export default handler;