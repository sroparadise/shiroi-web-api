const handler = async ({
    db,
    user,
}) => {
    if (!user) return {
        statusCode: 405,
        data: {
            message: 'user_not_logged_in',
        },
    };
    
    const [{ silk_own, silk_gift }] = await db.table('SK_Silk').select().where({
        JID: user.sub,
    });

    return {
        statusCode: 200,
        data: {
            silk_own,
            silk_point: silk_gift,
        },
    };
};

export default handler;