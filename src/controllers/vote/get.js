import config from '@lib/config';
import proxyCheck from '@lib/proxyCheck';

const handler = async (
    {
        db,
        user,
    },
    req,
) => {
    const ip = req.get('x-forwarded-for') || req.ip;
    const ipdetect = await proxyCheck(ip);

    if (!ipdetect) return {
        statusCode: 400,
        data: {
            message: 'bad_ipaddr',
        },
    };

    const site_config = config.vote.sites;
    const sites = Object.keys(site_config);
    const h_multi = 3600000;

    const [{ StrUserID }] = await db.table('TB_User').select('StrUserID').where({
        JID: user.sub,
    });

    const last_votes = await db
        .select(
            db.raw('DISTINCT (site_id)'),
            'timestamp',
            'ID'
        )
        .from('WEB_Vote').where({
            UserJID: user.sub,
        })
        .whereIn('site_id', sites)
        .orderBy('ID', 'desc');

    const get_vote = site => last_votes.find(i => i.site_id == site);

    const now = Date.now();

    const result = sites.map(site => {
        const { cooldown, url, reward } = site_config[site];
        const vote = get_vote(site);
        const cd = cooldown * h_multi;

        const last_vote = vote ? parseInt(vote.timestamp) : false;
        const next_vote = vote ? parseInt(vote.timestamp) + cd : now;

        return {
            site: site,
            url: `${url}${StrUserID}`,
            enabled: (next_vote <= now),
            last_vote,
            next_vote,
            reward,
        };
    });

    return {
        statusCode: 200,
        data: result,
    };
};

export default handler;
