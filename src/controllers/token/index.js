import get from './get';
import post from './post';
import put from './put';
//import delete from './delete';

export default {
    get,
    post,
    put,
    //delete,
};