import md5 from 'md5';
import jwt from '@lib/jwt';

const handler = async ({
    body: {
        username,
        password,
        remember,
    },
    db,
}) => {
    const [user] = await db.table('TB_User').where({
        StrUserID: username,
        password: md5(password),
    }).select();

    if (!user)  return {
        statusCode: 403,
        data: {
            message: `invalid_credentials`,
        }
    };

    if (user.EmailValidate == 0) return {
        statusCode: 405,
        data: {
            message: 'invalid_verification',
        },
    };

    const expires = remember ? 365 : 31;

    const token = jwt.create({
        iat: new Date(),
        sub: user.JID,
    }, expires);

    return {
        statusCode: 200,
        data: {
            token,
        },
    };
};
export default handler;
