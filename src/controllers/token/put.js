import emailValidator from 'email-validator';
import config from '@lib/config';
import md5 from 'md5';
import jwt from '@lib/jwt';
import { transporter } from '@lib/mailer';
import html from '@lib/templates/verification';

const error = message => ({
    statusCode: 400,
    data: {
        message,
    },
});

const handler = async ({
    user,
    body: {
        email,
        password,
    },
    db,
}) => {
    if (user.sub) {
        return error('user_logged_in');
    }

    if (!emailValidator.validate(email)) return error('email_invalid');

    if (password.length < 6) return error('password_invalid');

    try {
        const [{ JID, StrUserID, EmailValidate }] = await db.table('TB_User').where({
            Email: email,
            password: md5(password),
        }).select();

        if (!JID) return {
            statusCode: 403,
            data: {
                message: `invalid_credentials`,
            }
        };

        if (EmailValidate == 1) return {
            statusCode: 405,
            data: {
                message: 'email_already_verified',
            },
        };

        const token = jwt.create({
            iat: new Date(),
            sub: JID,
            verify: true,
        }, 30);
        
        // Send verification:
        await transporter.sendMail({
            from: `"Shiroi Online" <${config.SMTP.auth.user}>`,
            to: email,
            subject: `Shiroi Account Activation`,
            html: html({ username: StrUserID, token }),
        });

        return {
            statusCode: 200,
            data: {
                message: `verification_sent`,
            },
        };
    } catch (e) {
        return {
            statusCode: 403,
            data: {
                message: `invalid_credentials`,
            }
        }
    }
};

export default handler;
