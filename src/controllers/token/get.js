import jwt from '@lib/jwt';

const handler = async ({
    user: {
        sub,
        //verify,
        reset,
    },
    db,
}) => {
    if (!sub) return {
        statusCode: 400,
        data: {
            message: 'token_invalid',
        },
    };

    const [{ 
        JID, 
        StrUserID, 
        EmailValidate, 
    }] = await db.table('TB_User').where({
        JID: sub,
    }).select();

    if (!JID) return {
        statusCode: 404,
        data: {
            message: 'reference_not_found',
        },
    };

    if (EmailValidate == 1 && !reset) return {
        statusCode: 409,
        data: {
            message: 'already_verified',
        },
    };

    // Set user validated:
    if (!reset) {
        await db.table('TB_User').where({
            JID: sub,
        }).update({
            EmailValidate: 1,
        });
    }

    // Create token valid 365 days
    const token = jwt.create({
        sub: JID,
        reset: reset ? true : false,
    }, 365);

    return {
        statusCode: 200,
        data: {
            token,
            username: StrUserID,
        },
    };
};

export default handler;