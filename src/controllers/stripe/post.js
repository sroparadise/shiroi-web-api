import helpers from '@lib/payment';
import config from '@lib/config';
import moment from 'moment';

const handler = async ({
    rawBody,
    db,
}, req) => {
    const helper = new helpers.stripe(config.payments.providers.stripe);
    const { bonus } = config.payments;

    try {
        const signature = req.get('stripe-signature');

        const event = await helper.capture(rawBody, signature);

        if (event.type == 'checkout.session.completed') {

            const id = event.data.object.id;

            const [{ UserJID, reward }] = await db.table('WEB_Payments').select().where({
                payment_ref_init: id,
            });

            if (!UserJID || !reward) throw new Error('db_record_not_found');

            const [{ silk_own }] = await db.table('SK_Silk').select().where({
                JID: UserJID,
            });

            const [{ server_time }] = await db.raw('select CURRENT_TIMESTAMP as server_time');

            const update_time = moment(server_time).format("YYYY-MM-DD HH:mm:ss");

            const new_silk_amount = Math.round(parseInt(silk_own) + parseInt(reward) + parseInt(reward / 100 * bonus));

            await Promise.all([
                db.table('WEB_Payments').update({
                    updated_at: update_time,
                    payment_ref_complete: id,
                }).where({
                    UserJID,
                    payment_ref_init: id,
                    payment_ref_complete: null,
                    provider: 'stripe',
                }),
                db.table('SK_Silk').update({
                    silk_own: new_silk_amount,
                }).where({
                    JID: UserJID,
                }),
                db.table('SK_SilkChange_BY_Web').insert({
                    JID: UserJID,
                    silk_remain: new_silk_amount,
                    silk_offset: reward,
                    silk_type: 0,
                    reason: 0,
                }),
            ]);

            return {
                statusCode: 200,
                data: 'OK',
            };
        } else {
            throw new Error('handler_not_available');
        }
    } catch (e) {
        return {
            statusCode: 400,
            data: {
                message: e.message || 'unacceptable_webhook',
            },
        };
    }
};

export default handler;