import voteHandler from './shared/voteHandler';
import config from '@lib/config';

const handler = async ({
    db,
}, req) => {
    const ip = req.get('x-forwarded-for') || req.ip;
    const site = req.params.fn;
    
    let result = {
        message: `unable_to_serve`,
    };

    const { params } = config.vote.sites[site];

    if (!params) return {
        statusCode: 400,
        data: {
            message: 'bad_request'
        },
    };

    const [
        username,
        user_ip,
    ] = [
        req.query[params.username],
        req.query[params.user_ip],
    ];

    if (username && user_ip) result = await voteHandler(db, {
        site,
        username,
        ip: user_ip,
        request_ip: ip.split(':')[0],
    });

    return {
        statusCode: 200,
        data: result,
    };
};
export default handler;
