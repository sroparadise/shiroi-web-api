import voteHandler from './shared/voteHandler';

const handler = async ({
    db,
}, req) => {
    const ip = req.get('x-forwarded-for').split(':')[0] || req.ip;
    const site = req.params.fn || 'sro-servers';
    
    let result = {
        message: `unable_to_serve`,
    };
    
    const {
        userid,
        userip,
        voted,
    } = req.query;

    if (parseInt(voted) == 1) result = await voteHandler(db, {
        site,
        username: userid,
        ip: userip,
        request_ip: ip.split(':')[0],
    });

    return {
        statusCode: 200,
        data: result,
    };
};
export default handler;
