import config from '@lib/config';
import proxyCheck from '@lib/proxyCheck';

async function voteHandler(db, {
    site,
    username,
    ip,
    request_ip,
}) {
    const vote_sites = new Set(Object.keys(config.vote.sites));
    const whitelist = new Set(config.vote.whitelist);

    const ipdetect = await proxyCheck(ip);

    if (!ipdetect) return {
        message: 'bad_customer_ipaddr',
    };

    if (!vote_sites.has(site)) return {
        message: 'invalid_referer',
    };

    if (!whitelist.has(request_ip)) {
        console.log('provider_not_whitelisted', {
            site, request_ip,
        });
        return {
            message: 'invalid_referer_ip',
        };
    }

    const {
        cooldown,
        reward,
    } = config.vote.sites[site];

    const time_now = Date.now();
    const wait_time = cooldown * 3600000;

    const [result] = await db.table('TB_User').where({
        StrUserID: username
    }).select(['JID']);

    const { JID } = result;

    if (!JID) return {
        message: 'invalid_target_user',
    };

    const [last_vote] = await db.table('WEB_Vote').where({
        site_id: site,
        UserJID: JID,
    }).select().orderBy('ID', 'desc').limit(1);    

    const last_timestamp = last_vote ? parseInt(last_vote.timestamp) : time_now - wait_time - 1;
    const expire_timestamp = last_timestamp + wait_time;

    if (expire_timestamp > time_now) return {
        message: 'vote_cooldown_active',
        since: new Date(last_timestamp),
        until: new Date(expire_timestamp),
    };

    const [
        [next_vote],
        [{ silk_own }]
    ] = await Promise.all([
        db.table('WEB_Vote').insert({
            UserJID: JID,
            site_id: site,
            ip,
            reward,
            timestamp: time_now,
        }).returning('*'),
        db.table('SK_Silk').where({
            JID,
        }).select(),
    ]);

    const new_silk_amount = parseInt(silk_own) + parseInt(reward);

    await Promise.all([
        db.table('SK_Silk').update({
            silk_own: new_silk_amount,
        }).where({
            JID,
        }),
        db.table('SK_SilkChange_BY_Web').insert({
            JID,
            silk_remain: new_silk_amount,
            silk_offset: reward,
            silk_type: 2,
            reason: 0,
        }),
    ]);

    return {
        message: 'ok',
        next_vote: new Date(parseInt(next_vote.timestamp)),
    };
};

export default voteHandler;