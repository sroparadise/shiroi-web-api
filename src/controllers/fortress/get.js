const handler = async ({
    db,
}) => {
    const data = await db.raw('exec WEB_GetFortressInfo');

    const index = {
        'FORTRESS_CONSTANTINOPLE': 'Constantinopole',
        'FORTRESS_HOTAN': 'Hotan',
        'FORTRESS_JANGAN': 'Jangan',
        'FORTRESS_BIJEOKDAN': 'Bandit Mountain'
    }

    return {
        statusCode: 200,
        data: data.map(({
            code,
            icon,
            guild_name,
            tax_ratio,
            tax,
        }) => ({
            name: index[code],
            icon: `/sro/icon/${icon.replace(`\\`, `/`)}`,
            guild_name,
            tax_ratio,
            tax,
        })),
    };
};
export default handler;
