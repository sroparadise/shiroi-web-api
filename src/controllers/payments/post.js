import helpers from '@lib/payment';
import config from '@lib/config';

const error = message => ({
    statusCode: 400,
    data: {
        message,
    },
});

const handler = async ({
    user,
    body,
    db,
}) => {
    if (!body.agreement) return error('invalid_agreement');
    try {
        const provider_config = config.payments.providers[body.provider];
        const helper = new helpers[body.provider](provider_config);
        const package_details = config.payments.packages.find(i => i.id == body.package);

        if (!helper) return error('invalid_payment_provider');

        if (!package_details) return error('invalid_package');

        const { session_ref, data } = await helper.init({
            custom_id: user.sub,
            cost: package_details.cost,
            reward: package_details.amount,
            package_code: package_details.id,
            package_name: package_details.name,
        });

        await db.table('WEB_Payments').insert({
            UserJID: user.sub,
            provider: body.provider,
            payment_ref_init: session_ref,
            payment_amount: package_details.cost.toFixed(2),
            reward: package_details.amount,
        });

        return {
            statusCode: 200,
            data: {
                provider: body.provider,
                result: data,
            },
        };
    } catch (e) {
        return error('invalid_payment_request');
    }
};

export default handler;