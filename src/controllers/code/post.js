import proxyCheck from '@lib/proxyCheck';

const error = message => ({
    statusCode: 400,
    data: {
        message,
    },
});

const handler = async (
    {
        user,
        body: {
            code,
        },
        db,
    },
    req,
) => {
    const ip = req.get('x-forwarded-for') || req.ip;
    const ipdetect = await proxyCheck(ip);

    if (!ipdetect) return error('bad_ipaddr');

    try {
        if (code.length < 6 || code.length > 32) return error('invalid_code');

        if (!user.sub) return error('unauthorized');

        const [{ reward }] = await db.raw("exec WEB_ClaimBonusCode ?, ?, ?", [user.sub, ip.split(':')[0], code]);

        if (reward && reward > 0) {
            const [{ silk_own }] = await db.table('SK_Silk').select().where({
                JID: user.sub,
            });

            const new_silk_amount = parseInt(silk_own) + parseInt(reward);

            await Promise.all([
                db.table('SK_Silk').update({
                    silk_own: new_silk_amount,
                }).where({
                    JID: user.sub,
                }),
                db.table('SK_SilkChange_BY_Web').insert({
                    JID: user.sub,
                    silk_remain: new_silk_amount,
                    silk_offset: reward,
                    silk_type: 0,
                    reason: 0,
                }),
            ]);

            return {
                statusCode: 200,
                data: {
                    reward,
                },
            };
        } else {
            return error('validation_failed');
        }
    } catch (e) {
        console.log(e)
        return error('unknown_error');
    }
};

export default handler;