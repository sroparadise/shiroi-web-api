import status from './status';
import users from './users';
import token from './token';
import time from './time';
import credits from './credits';
import news from './news';
import fortress from './fortress';
import schedule from './schedule';
import password from './password';
import packages from './packages';
import payments from './payments';
import paypal from './paypal';
import stripe from './stripe';
import ranking from './ranking';
import postback from './postback';
import vote from './vote';
import code from './code';
import info from './info';

export default {
    status,
    users,
    token,
    time,
    credits,
    news,
    fortress,
    schedule,
    password,
    packages,
    payments,
    paypal,
    stripe,
    ranking,
    postback,
    vote,
    code,
    info,
};