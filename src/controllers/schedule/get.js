import moment from 'moment';

const handler = async ({
    db,
}) => {
    const [
        data, 
        [{ server_time }]
    ] = await Promise.all([
        db.raw('exec WEB_GetServerSchedule'),
        db.raw('select CURRENT_TIMESTAMP as server_time'),
    ]);


    return {
        statusCode: 200,
        data: {
            schedules: data,
            timestamp: moment(server_time).utc(),
        },
    };
};
export default handler;
