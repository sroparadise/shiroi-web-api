import config from '@lib/config';

const handler = async () => {
    return {
        statusCode: 200,
        data: {
            packages: config.payments.packages,
            bonus: config.payments.bonus,
        },
    };
};

export default handler;