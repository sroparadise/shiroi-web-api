const handler = async ({
    db,
}) => {
    const news_data = await db.table('_Notice').select().limit(10).orderBy('ID', 'desc');

    return {
        statusCode: 200,
        data: news_data.map(({
            Subject,
            Article,
            EditDate
        }) => ({
            subject: Subject,
            content: Article,
            date: EditDate,
        })),
    };
};
export default handler;
