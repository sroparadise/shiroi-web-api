import helpers from '@lib/payment';
import config from '@lib/config';

const handler = async ({
    body: {
        event_type,
        resource,
    },
    db,
}) => {
    const helper = new helpers.paypal(config.payments.providers.paypal);

    const { bonus } = config.payments;

    try {
        switch (event_type) {
            case 'PAYMENT.CAPTURE.PENDING':
            case 'CHECKOUT.ORDER.APPROVED':
                await helper.capture(resource.id);
                return {
                    statusCode: 200,
                    data: 'OK',
                };
            case 'PAYMENT.SALE.COMPLETED':
            case 'PAYMENT.CAPTURE.COMPLETED':
                const { id, custom_id, update_time } = resource;

                const [
                    [{ reward }],
                    [{ silk_own }],
                ] = await Promise.all([
                    db.table('WEB_Payments').update({
                        updated_at: update_time,
                        payment_ref_complete: id,
                    }).where({
                        UserJID: custom_id,
                        payment_amount: resource.amount.value,
                        provider: 'paypal',
                        payment_ref_complete: null
                    }).returning('*'),
                    db.table('SK_Silk').select().where({
                        JID: custom_id,
                    }),
                ]);

                const new_silk_amount = Math.round(parseInt(silk_own) + parseInt(reward) + parseInt(reward / 100 * bonus));

                await Promise.all([
                    db.table('SK_Silk').update({
                        silk_own: new_silk_amount,
                    }).where({
                        JID: custom_id,
                    }),
                    db.table('SK_SilkChange_BY_Web').insert({
                        JID: custom_id,
                        silk_remain: new_silk_amount,
                        silk_offset: reward,
                        silk_type: 0,
                        reason: 0,
                    }),
                ]);

                return {
                    statusCode: 200,
                    data: 'OK',
                };
            default:
                throw new Error('handler_not_available');
        }

    } catch (e) {
        return {
            statusCode: 400,
            data: {
                message: e.message || 'unacceptable_webhook',
            },
        };
    }
};

export default handler;