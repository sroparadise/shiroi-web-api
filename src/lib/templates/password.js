export default ({ username, token }) => `<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html charset=UTF-8" />
</head>
<body>
    <table width="648" cellpadding="0" cellspacing="0" style="border: 1px solid #ccc;background:#e5e5e5;">
        <tr>
            <td style="background:#fff;padding:20px 10px;">
                Hi <b>${username}</b>,<br>
                We've received a password recovery request on this account.
                <br>
                <br>
                <br>
                <b>Please <a href="https://shiroi.online/reset/${token}">click here</a> to change your password.</b>
                <br>
                <br>
                <br>
                <b>Please Note</b> if this wasn't requested by you - just ignore the email and re-check your security details.
                <br>
                <br>
                <br>
                Best regards,
                <br>
                <b>Shiroi Online</b>
                <br>
                <br>
                <a href="https://shiroi.online">Website</a> | <a href="https://discord.com/invite/AmdD2mxQTq">Forums</a>
                <br>
                <br>
                <br>
                <img src="https://shiroi.online/icon.png" style="width: 32px;height: 32px;" />
            </td>
        </tr>
    </table>
</body>
</html>
`;