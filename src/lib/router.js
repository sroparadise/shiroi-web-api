import services from '@control/index';
import jwt from '@lib/jwt';
import config from '@lib/config';

const router = async (database, req, res) => {
    const {
        params: {
            index,
        },
        method,
        body,
        rawBody,
    } = req;

    const action = method.toLowerCase();

    let user = {
        sub: null,
        rights: null,
    };

    if (services[index] && services[index][action]) {
        const check_authorization = !(config.AUTH_WHITELIST[index] && config.AUTH_WHITELIST[index].has(method));
        const auth_header = req.get('authorization');

        if (auth_header && check_authorization) {
            const [token_type, token] = auth_header.split(' ');
            let validation;

            try {
                validation = await jwt.read(token);
            } catch (e) {
                return res.status(401).send({
                    message: e.replace(" ", "_"),
                });
            }

            user = validation.body;

            try {
                const [access] = await database.table('TB_User').where({
                    JID: user.sub,
                }).select(['sec_primary']);
                if (access && access.sec_primary) user.rights = access.sec_primary;
            } catch (e) {
                return res.status(401).send({
                    message: 'USER_NOT_FOUND',
                });
            }
        }

        try {
            const { statusCode, data } = await services[index][action]({
                user,
                body,
                rawBody,
                db: database,
                params: req.params,
                query: req.query,
            }, req, res);
            return res.status(statusCode).send(data);
        } catch (e) {
            console.log(`API error:`, {
                body: req.body,
                index,
                method,
                err: e.message,
            });

            return res.status(500).send({
                message: `request_invalid`,
            });
        }
    } else return res.status(404).send({
        message: `resource_not_found`
    });
};

export default router;