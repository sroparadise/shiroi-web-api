import dotenv from 'dotenv';

dotenv.config();

const {
    SMTP_USERNAME,
    SMTP_PASSWORD,
    SQL_HOST,
    SQL_PORT,
    SQL_USERNAME,
    SQL_PASSWORD,
    MAX_SERVER_CAPACITY,
    JWT_SECRET,
    JWT_SECRET_EXPIRES,
    LISTEN_PORT,
    PAY_STRIPE_ID,
    PAY_STRIPE_SECRET,
    PAY_PAYPAL_SECRET,
    PAY_PAYPAL_WEBHOOK,
    SENDGRID_KEY,
    NODE_ENV,
} = process.env;

export default {
    PORT: NODE_ENV == 'development' ? 3000 : LISTEN_PORT,
    GAME: {
        capacity: MAX_SERVER_CAPACITY,
    },
    SMTP: {
        host: 'smtp.gmail.com',
        port: 465,
        secure: true,
        auth: {
            user: SMTP_USERNAME,
            pass: SMTP_PASSWORD,
        },
    },
    SMTP_ALT: {
        host: 'smtp.sendgrid.net',
        port: 465,
        secure: true,
        auth: {
            user: `apikey`,
            pass: SENDGRID_KEY,
        },
    },
    SQL: {
        host: SQL_HOST,
        port: SQL_PORT,
        username: SQL_USERNAME,
        password: SQL_PASSWORD,
    },
    JWT: {
        secret: JWT_SECRET,
        expiresIn: JWT_SECRET_EXPIRES || false,
        algorhytm: 'HS256',
    },
    vote: {
        whitelist: [
            '::1',
            '::ffff:127.0.0.1',
            '127.0.0.1', 
            '193.70.122.73', // xtremetop100
            '116.203.217.217', // silkroad-servers
            '192.99.101.31', // topg
            '51.81.81.226', //arena-top100
            '162.0.229.136', //silkroadtop100
        ],  
        sites: {
            'sro-servers': {
                url: `https://silkroad-servers.com/index.php?a=in&u=shiroionline&id=`,
                cooldown: 12, // usually 2-24 hours (depends on site)
                reward: 5, // silk given
            },
            // 'xtremetop100': {
            //     url: `https://www.xtremetop100.com/in.php?site=1132371318&postback=`,
            //     cooldown: 12, // usually 2-24 hours (depends on site)
            //     reward: 5, 
            //     params: {
            //         username: 'custom',
            //         user_ip: 'votingip',
            //     },
            // },
            'top-g': {
                url: `https://topg.org/silkroad-private-servers/in-625705-`,
                cooldown: 12,
                reward: 5,
                params: {
                    username: 'p_resp',
                    user_ip: 'ip',
                },
            },
            'arena-top100': {
                url: `https://www.arena-top100.com/index.php?a=in&u=Shiroi&id=`,
                cooldown: 12,
                reward: 5,
                params: {
                    params: {
                        username: 'userid',
                        userip: 'userip'
                    }
                }
            },
            // 'silkroadtop100': {
            //     url: `https://silkroadtop100.com/?p=vote&id=100178&user=`,
            //     cooldown: 12,
            //     reward: 3,
            //     params: {
            //         params: {
            //             username: 'user',
            //             userip: 'votingip',
            //         },
            //     }
            // }
        },
    },
    payments: {
        providers: {
            paypal: {
                mode: 'live',
                client_id: PAY_STRIPE_ID,
                client_secret: PAY_STRIPE_SECRET,
            },
            stripe: {
                client_secret: PAY_PAYPAL_SECRET,
                webhook: PAY_PAYPAL_WEBHOOK,
            }
        },
        // silk bonus:
        bonus: 50,
        // Available silk purchase packages:
        packages: [
            {
                id: 'PACKAGE_S_700',
                name: 'Silk x 700',
                cost: 6.50,
                amount: 700,
            },
            {
                id: 'PACKAGE_S_1100',
                name: 'Silk x 1100',
                cost: 10.00,
                amount: 1100,
            },
            {
                id: 'PACKAGE_S_27500',
                name: 'Silk x 2700',
                cost: 25.00,
                amount: 2700,
            },
            {
                id: 'PACKAGE_S_5500',
                name: 'Silk x 5500',
                cost: 50.00,
                amount: 5500,
            },
            {
                id: 'PACKAGE_S_12K',
                name: 'Silk x 12000',
                cost: 100.00,
                amount: 12000,
            },
            {
                id: 'SPECIAL_PACKAGE',
                name: 'Special 24K Silk Package',
                cost: 150.00,
                amount: 24000,
            },
        ],
    },
    // Routes / methods that require no authentication:
    AUTH_WHITELIST: {
        'status': new Set([
            'GET', // server capacity
        ]),
        'token': new Set([
            'POST', // user login
            'PUT',  // Resend Verification
        ]),
        'users': new Set([
            'POST', // Registration
            'PUT', // Password Recovery
        ]),
        'time': new Set([
            'GET' // Server time
        ]),
        'news': new Set([
            'GET' // Server News
        ]),
        'fortress': new Set([
            'GET' // Fortress Info
        ]),
        'schedule': new Set([
            'GET' // Server schedule
        ]),
        'paypal': new Set([
            'POST' // PayPal Webhook
        ]),
        'stripe': new Set([
            'POST' // Stripe Webhook
        ]),
        'packages': new Set([
            'GET' // Silk Packages
        ]),
        'ranking': new Set([
            'GET', // Ranking
            'POST', // Search player 
        ]),
        'postback': new Set([
            'GET',
            'POST',
        ]),
    },
};