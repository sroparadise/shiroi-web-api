import {  isIPv4 } from 'net';
import portScan from './portScan';

const scan_ports = [80, 8080, 443];

const check = async ip => {

    const split_ip = ip.split(':')[0]
    const scan_ip = isIPv4(split_ip) ? split_ip : ip;
    const promises = scan_ports.map(async port => await portScan(scan_ip, port, 1000));

    try {
        const scan_results = await Promise.all(promises);

        // console.log({
        //     scan_ip,
        //     split_ip,
        //     scan_results,
        // });

        if (scan_results.filter(i => i !== false).length) throw 'WEBSERVER_DETECTED';

        return true;
    } catch (error) {
        
        // console.log({
        //     error,
        //     scan_ip,
        //     split_ip,
        // });

        return false;
    }
};

export default check;