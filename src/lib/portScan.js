import { Socket } from 'net';

export default (host, port, timeout = 1000) => new Promise(
    (resolve) => {
        const socket = new Socket();

        const onHidden = () => { 
            if (socket) socket.destroy();
            resolve(false);
        };
        
        const onOpen = () => {
            socket.destroy();
            resolve(true);
        };

        socket.on('connect', onOpen);

        ['timeout', 'error', 'close'].map(i => socket.on(i, onHidden));

        socket.setTimeout(timeout);
        socket.connect(port, host);
    }
);
