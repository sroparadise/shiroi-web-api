import knex from 'knex';
import config from '@lib/config';

const {
    host,
    username,
    password,
} = config.SQL;

const adapter = (database = 'SRO_R_ACCOUNT') => {
    try {
        return knex({
            client: 'mssql',
            connection: {
                database,
                server: host,
                user: username,
                password,
                options: {
                    enableArithAbort: true,
                },
            },
        });
    } catch (e) {
        console.log(`Database connection failed!`, e);
    }
}

export default adapter;
