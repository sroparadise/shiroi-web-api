import moment from 'moment';
import { v4 as uuid } from 'uuid';

class GameEvents {
    constructor(data, limit = 50) {
        this.data = data;
        this.limit = limit;

        this.time_now = moment(new Date());

        this.start = {
            weekly: moment(this.time_now).utc().startOf('week'),
            daily: moment(this.time_now).utc().startOf('day'),
        };

        this.types = {
            1: 'daily',
            3: 'weekly',
        };
    }

    getPeriod(type) {
        switch (this.types[type]) {
            case this.types[1]:
                return 1;
            case this.types[3]:
                return 7;
        }
    }

    getSummary({
        type,
        day,
        hour,
        minute,
        second,
    }) {
        const period = this.getPeriod(type);
        const count_from = this.start[this.types[type]];
        const time_to_happen = moment(count_from).add(day == 0 ? day : day - 1, 'days').add(hour, 'hours').add(minute, 'minutes').add(second, 'seconds');
        const time_to_happen_next = moment(time_to_happen).add(period, 'days');
        const upcoming_event_time = moment(time_to_happen).isBefore(this.time_now) ? time_to_happen_next : time_to_happen;
        const difference = moment(upcoming_event_time).diff(this.time_now);

        const {
            _data: {
                days,
                hours,
                minutes,
                seconds,
            },
        } = moment.duration(difference);

        return {
            type: this.types[type],
            ms: difference,
            at: upcoming_event_time,
            countdown: {
                days,
                hours,
                minutes,
                seconds,
            },
        };
    }

    estimate() {
        return this.data.reduce((arr, {
            MainInterval_Type: type,
            ScheduleName: desc,
            SubInterval_DayOfWeek: day,
            SubInterval_StartTimeHour: hour,
            SubInterval_StartTimeMinute: minute,
            SubInterval_StartTimeSecond: second,
            SubInterval_DurationSecond: duration,
        }) => ([
            ...arr,
            {
                desc,
                duration,
                ...this.getSummary({
                    type,
                    day,
                    hour,
                    minute,
                    second,
                }),
                repeats: {
                    day,
                    hour,
                    minute,
                    second,
                },
            }
        ]), []);
    }

    o(i) {
        return i > 9 ? i : `0${i}`;
    }

    sort() {
        return this.estimate().sort((a, b) => a.ms - b.ms).filter(i => (i.desc)).slice(0, this.limit);
    }

    result() {
        return this.sort().map(({
            desc,
            duration,
            type,
            at,
            countdown: {
                days,
                hours,
                minutes,
                seconds,
            }
        }) => {
            const {
                _data: t,
            } = moment.duration(duration * 1000);
            return {
                id: uuid(),
                type,
                name: desc,
                start_time: moment(at).format('YYYY/MM/DD HH:mm:ss'),
                duration: `${this.o(t.hours)}:${this.o(t.minutes)}:${this.o(t.seconds)}`,
                tts: `${this.o(hours)}:${this.o(minutes)}:${this.o(seconds)}`,
            };
        });
    }
}

export default GameEvents;