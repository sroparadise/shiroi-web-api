import config from '@lib/config';
import nodemailer from 'nodemailer';

const transporter = nodemailer.createTransport(config.SMTP);
const transporter_alt = nodemailer.createTransport(config.SMTP_ALT);

export {
    transporter,
    transporter_alt,
    nodemailer,
};