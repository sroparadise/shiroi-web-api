import stripe from 'stripe';

class Stripe {
    constructor(config) {
        Object.assign(this, config);
        this.client = this.initClient();
    }

    initClient() {
        return stripe(this.client_secret);
    }

    async init({
        currency_code = 'gbp',
        custom_id,
        cost,
        reward,
        package_code,
        package_name,
    }) {
        const {
            id,
        } = await this.client.checkout.sessions.create({
            submit_type: 'donate',
            payment_method_types: [
                'card', 
            ],
            client_reference_id: custom_id,
            line_items: [{
                name: `${package_code}`,
                description: `Voluntary donation of total ${currency_code.toUpperCase()}${cost.toFixed(2)} for ${package_name}.`,
                amount: cost * 100,
                currency: currency_code,
                quantity: 1
            }],
            success_url: 'https://shiroi.online/donate?complete=1&session_id={CHECKOUT_SESSION_ID})',
            cancel_url: 'https://shiroi.online/donate?complete=0'
        });
        return {
            session_ref: id,
            data: id,
        };
    }

    async capture(body, signature) {
        return this.client.webhooks.constructEvent(
            body,
            signature,
            this.webhook,
        );
    }

    async retrieveIntent(id) {
        return this.client.paymentIntents.retrieve(id);
    }

}

export default Stripe;