import paypal from '@paypal/checkout-server-sdk';

class PayPal {
    constructor(config) {
        Object.assign(this, config);
        this.client = this.initClient();
    }

    initClient() {
        return new paypal.core.PayPalHttpClient(this.initEnv());
    }

    initEnv() {
        switch (this.mode) {
            case 'live':
                return new paypal.core.LiveEnvironment(this.client_id, this.client_secret);
            case 'sandbox':
                return new paypal.core.SandboxEnvironment(this.client_id, this.client_secret);
        }
    }

    initCaptureRequest(id) {
        return new paypal.orders.OrdersCaptureRequest(id);
    }

    initOrdersRequest() {
        return new paypal.orders.OrdersCreateRequest();
    }

    async init({
        currency_code = 'GBP',
        custom_id,
        cost,
        reward,
        package_code,
        package_name,
    }) {
        const {
            result
        } = await this.client.execute(
            this.initOrdersRequest().requestBody({
                intent: `CAPTURE`,
                application_context: {
                    return_url: `https://shiroi.online/donate?completed=1`,
                    cancel_url: `https://shiroi.online/donate?completed=0`
                },
                purchase_units: [{
                    custom_id,
                    amount: {
                        currency_code,
                        value: cost.toFixed(2),
                        breakdown: {
                            item_total: {
                                currency_code,
                                value: cost.toFixed(2)
                            },
                        },
                    },
                    items: [{
                        name: `${reward} tokens`,
                        description: `Voluntary donation of total $${cost.toFixed(2)} for ${package_name}.`,
                        sku: package_code,
                        unit_amount: {
                            currency_code,
                            value: cost.toFixed(2)
                        },
                        quantity: 1,
                        category: `DIGITAL_GOODS`
                    }],
                }],
            })
        );
        
        const  { id, links } = result;

        const redirect_url = links.find(i => i.rel == 'approve');

        return {
            session_ref: id,
            data: redirect_url.href,
        };
    }

    async capture(data) {
        const { result } = await this.client.execute(
            this.initCaptureRequest(data).requestBody({})
        );
        return {
            data: result,
        }
    }

}

export default PayPal;