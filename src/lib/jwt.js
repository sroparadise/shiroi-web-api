import config from './config';
import jwt from 'njwt';

const { secret, algorhytm } = config.JWT;

const err = e => {
    throw e.userMessage.replace(' ', '_').toLowerCase();
};

const create = (data, days = 365) => {
    try {
        const token = jwt.create(data, secret, algorhytm);

        token.setExpiration(new Date().getTime() + (3600 * 1000 * 24 * days));

        return token.compact();
    } catch (e) {
        err(e);
    }
};

const read = token => {
    try {
        const result = jwt.verify(token, secret, algorhytm);
        return result;
    } catch (e) {
        err(e);
    }
};

export default {
    create,
    read,
};