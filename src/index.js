import bodyParser from 'body-parser';
import express from 'express';
import sql from '@lib/sql';
import config from '@lib/config';
import router from '@lib/router';
import helmet from 'helmet';
import cors from 'cors';

const {
    PORT
} = config;

const app = express();
const database = sql();

app.set('trust proxy', true);

app.use((_, res, next) => {
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
});

app.use(cors());
app.use(helmet({
    contentSecurityPolicy: false,
}));

app.use(express.static('public'));

app.use(bodyParser.json({
    verify: (req, _, buf) => {
        req.rawBody = buf;
    }
}));


async function reRoute(req, res) {
    try {
        await router(database, req, res);
    } catch (e) {
        return res.status(500).send({
            message: `did_not_work`,
        });
    }
}

app.all('/v1/:index', reRoute);
app.all('/v1/:index/:fn', reRoute);
app.all('/v1/:index/:fn/:args', reRoute);

app.listen(PORT, () => {
    console.log(`API Ready: http://localhost:${PORT}`);
});