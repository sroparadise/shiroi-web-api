import config from '@lib/config';
import sql from '@lib/sql';
import { transporter_alt as transporter } from '@lib/mailer';
import html from '@lib/templates/halloween';
import emailValidator from "email-validator";
import { uniqBy } from 'lodash';

const db = sql();

/**
 * Use this file for bulk email sending `yarn mailer`
 */

async function run() {
    try {
        const results = await db.raw('exec WEB_GetMailingList');
        const unique = uniqBy(results, 'email');

        const filtered = await unique.filter(async i => emailValidator.validate(i.email));

        for await (const {email, name} of filtered) {
            try {
                await transporter.sendMail({
                    from: `"Shiroi Online" <${config.SMTP.auth.user}>`,
                    to: email,
                    subject: `Are you ready for the Great Halloweener?`,
                    html: html({ username: name }),
                    headers: {
                        "x-priority": "1",
                        "x-msmail-priority": "High",
                        "importance": "high"
                    },
                });
                console.log(`Sent:`, { email, name });
            } catch (e) {
                console.log(`Failed to email:`, { email, name });
            }
        }

        console.log(`Finished Sending`);
    } catch (e) {
        console.log(`Errored Sending`, e);
    } finally {
        process.exit(0);
    }

}

run();