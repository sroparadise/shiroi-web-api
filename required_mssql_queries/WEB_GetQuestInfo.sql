USE SRO_R_ACCOUNT
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER PROCEDURE WEB_GetQuestInfo
AS
BEGIN
SELECT 
	Level as 'Required Level', 
	sn.English as 'Quest Name', 
	qr.Exp as 'EXP reward',
	qr.SP as 'SP Reward',
	qr.SPExp as 'SP EXP Reward',
	qr.Gold as 'Gold Reward',
	(CASE qr.IsItemReward
		WHEN 1 THEN 'yes'
		WHEN 0 THEN 'no'
	END) as 'Rewards Items'
FROM 
	SRO_R_SHARD.dbo._RefQuest rq
	LEFT JOIN SRO_R_DESIGN.dbo.TextQuest_SpeechName sn ON sn.Codename128 COLLATE DATABASE_DEFAULT = rq.NameString  COLLATE DATABASE_DEFAULT 
	LEFT JOIN SRO_R_SHARD.dbo._RefQuestReward qr on qr.QuestID = rq.ID
WHERE 
	rq.CodeName NOT LIKE 'QNQ_LV%' 
	AND rq.CodeName NOT LIKE 'QTUTORIAL%' 
	AND rq.CodeName NOT LIKE 'QEV%'
	AND rq.CodeName NOT LIKE '%TUTORIAL%'
	AND rq.CodeName NOT LIKE 'QNO_LV%' 
	AND rq.Service = 1
	AND sn.English is not null
ORDER BY 
	LEVEL ASC
END