USE [SRO_R_ACCOUNT]
GO
/****** Object:  StoredProcedure [dbo].[WEB_GetServerSchedule]    Script Date: 2/16/2021 3:22:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[WEB_GetServerSchedule]
AS
BEGIN
	SELECT 
		rsd.ScheduleName,
		s.MainInterval_Type, 
		s.MainInterval_TypeDate,
		s.SubInterval_DayOfWeek,
		s.SubInterval_Days,
		s.SubInterval_DurationSecond,
		s.SubInterval_MaintainTime,
		s.SubInterval_Months,
		s.SubInterval_RepititionTerm,
		s.SubInterval_StartTimeHour,
		s.SubInterval_StartTimeMinute,
		s.SubInterval_StartTimeSecond
	FROM SRO_R_SHARD.dbo._Schedule s
	LEFT JOIN SRO_R_SHARD.dbo._RefScheduleDefine rsd
	ON s.ScheduleDefineIdx = rsd.ScheduleDefineIdx
END