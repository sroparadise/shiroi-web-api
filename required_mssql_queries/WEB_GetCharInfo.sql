USE [SRO_R_ACCOUNT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[WEB_GetCharInfo]
	@CharName VARCHAR(32)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT 
	c.CharName16 as Name, c.RefObjID as Icon, c.MaxLevel as Level, c.HP, c.MP, c.Intellect, c.Strength, c.RemainGold as Gold
FROM SRO_R_SHARD.dbo._Char c
WHERE CharName16 = @CharName
END
