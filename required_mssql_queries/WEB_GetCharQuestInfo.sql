USE SRO_R_SHARD

DECLARE @character VARCHAR(32) = 'Redemption'

SELECT 
	@character as 'Char',
	Level as 'Required Level', 
	sn.English as 'Quest Name', 
	(CASE Status
		WHEN 1 THEN '-'
		WHEN 4 THEN 'completed'
		WHEN 5 THEN '-'
		WHEN 6 THEN 'cancelled'
		WHEN 7 THEN '-'
		WHEN 8 THEN 'in progress'
	END) as 'Progression',
	qr.Exp as 'EXP reward',
	qr.SP as 'SP Reward',
	qr.SPExp as 'SP EXP Reward',
	qr.Gold as 'Gold Reward'
FROM 
	_CharQuest cq
	LEFT JOIN _RefQuest rq ON rq.ID = cq.QuestID
	LEFT JOIN SRO_R_DESIGN.dbo.TextQuest_SpeechName sn ON sn.Codename128 COLLATE DATABASE_DEFAULT = rq.NameString  COLLATE DATABASE_DEFAULT 
	LEFT JOIN _RefQuestReward qr on qr.QuestID = rq.ID
WHERE 
	CharID = (SELECT CharID FROM _Char WHERE CharName16 = @character) 
	AND rq.CodeName NOT LIKE 'QNQ_LV%' 
	AND rq.CodeName NOT LIKE 'QTUTORIAL%' 
	AND rq.CodeName NOT LIKE 'QEV%'
	AND rq.CodeName NOT LIKE '%TUTORIAL%'
	AND rq.CodeName NOT LIKE 'QNO_LV%' 
ORDER BY 
	LEVEL ASC