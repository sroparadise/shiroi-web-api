USE [SRO_R_ACCOUNT]
GO
/****** Object:  StoredProcedure [dbo].[WEB_ClaimBonusCode]    Script Date: 10/25/2021 17:35:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[WEB_ClaimBonusCode]
	@JID bigint,
	@IP varchar(100),
	@code varchar(30)
AS
BEGIN
	declare @codeReward int;
	declare @canUse int;
	declare @codeID int;

	select @codeReward = COALESCE(reward, 0) from _BonusCodes WHERE code = @code and expired = 0
	select @codeID = coalesce(ID, 0) from _BonusCodes where code = @code;
	select @canUse = coalesce(COUNT(*), 0) from _BonusCodeLog WHERE code = @codeID and (IP = @IP OR JID = @JID)

	if (@codeReward > 0 and @canUse = 0) BEGIN
		INSERT _BonusCodeLog(IP, code, JID) values (@IP, @codeID, @JID);

		SELECT @codeReward as reward;
	END
	ELSE BEGIN
		SELECT 0 AS reward;
	END
END
