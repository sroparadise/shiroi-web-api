-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE OR ALTER PROCEDURE WEB_GetJobRanking
	@count INT = 100,
	@jobtype INT = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT TOP (@count)
	n.NickName16,
	JobLevel,
	JobExp,
	ReputationPoint,
	KillCount,
	CASE j.Class WHEN 0 THEN 'Thief' WHEN 1 THEN 'Hunter' END AS JobType,
	Rank

  FROM SRO_R_SHARD.dbo._CharTradeConflictJob j
  LEFT JOIN SRO_R_SHARD.dbo._Char c on j.CharID = c.CharID
  LEFT JOIN SRO_R_SHARD.dbo._CharNickNameList n on n.CharID = c.CharID
  WHERE n.NickName16 is not null

  AND c.CharID NOT IN (
		SELECT CharID FROM SRO_R_SHARD.dbo._User WHERE UserJID IN (
			SELECT JID FROM TB_User WHERE sec_primary IN (1, 2)
		)
	) 

	AND c.CharID NOT IN (
		SELECT CharID FROM SRO_R_SHARD.dbo._User WHERE UserJID IN (
			SELECT UserJID FROM _Punishment WHERE BlockEndTime >= CURRENT_TIMESTAMP
		)
	)

	AND c.CharID > 0 

	AND cast(c.LastLogout as DATE) >= dateadd(dd,-3, cast(CURRENT_TIMESTAMP as DATE))  -- 3 days ago only

	AND j.Class = @jobtype

  ORDER BY Rank DESC, ReputationPoint DESC, KillCount DESC, JobLevel DESC, JobExp DESC
END
GO