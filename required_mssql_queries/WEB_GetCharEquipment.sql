USE [SRO_R_ACCOUNT]
GO
/****** Object:  StoredProcedure [dbo].[_GetCharEquipment]    Script Date: 07/11/2020 14:47:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[WEB_GetCharEquipment]
	@CharName VARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;
SELECT 
CASE inv.Slot
	WHEN 0 THEN 'helm'
    WHEN 1 THEN 'body'
	WHEN 2 THEN 'shoulder'
	WHEN 3 THEN 'hands'
	WHEN 4 THEN 'legs'
	WHEN 5 THEN 'foot'
	WHEN 6 THEN 'weapon'
	WHEN 9 THEN 'earring'
	WHEN 10 THEN 'necklace'
	WHEN 11 THEN 'left_ring'
	WHEN 12 THEN 'right_ring'
END AS slot,
ref.CodeName128,
ref.NameStrID128,
REPLACE(REPLACE(ref.AssocFileIcon128, '\', '/'), '.ddj', '.jpg') as icon,
i.OptLevel as plus_value, 
CASE 
	WHEN ref.CodeName128 LIKE '%A_RARE' THEN 'SOS'
	WHEN ref.CodeName128 LIKE '%B_RARE' THEN 'SOM'
	WHEN ref.CodeName128 LIKE '%C_RARE' THEN 'SUN'
	ELSE 'BASIC'
END as rarity,
CASE 
	WHEN ref.CodeName128 LIKE '%_SET_%' THEN 'yes'
	ELSE 'no'
END as set_item,
	i.Variance,
	i.MagParam1,
	i.MagParam2,
	i.MagParam3,
	i.MagParam4,
	i.MagParam5,
	i.MagParam6,
	i.MagParam8,
	i.MagParam9,
	i.MagParam10,
	i.MagParam11,
	i.MagParam12
FROM _Inventory inv
LEFT JOIN SRO_R_SHARD.dbo._Items i on i.ID64 = inv.ItemID
LEFT JOIN SRO_R_SHARD.dbo._RefObjCommon as ref on ref.ID=i.RefItemID
WHERE CharID = (SELECT cnl.CharID FROM SRO_R_SHARD.dbo._CharNameList cnl WHERE cnl.CharName16 = @CharName) AND inv.itemID > 0 AND inv.Slot IN (0, 1, 2, 3, 4, 5, 6, 9, 10, 11, 12)
ORDER BY inv.Slot

END
