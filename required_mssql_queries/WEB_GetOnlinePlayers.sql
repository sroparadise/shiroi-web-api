USE [SRO_R_ACCOUNT]
GO
/****** Object:  StoredProcedure [dbo].[WEB_GetOnlinePlayers]    Script Date: 2/16/2021 3:21:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[WEB_GetOnlinePlayers]
AS
SELECT TOP 1 nUserCount as nUserCount FROM _ShardCurrentUser ORDER BY dLogDate DESC