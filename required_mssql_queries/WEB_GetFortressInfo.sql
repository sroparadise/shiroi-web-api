USE [SRO_R_ACCOUNT]
GO
/****** Object:  StoredProcedure [dbo].[WEB_GetFortressInfo]    Script Date: 2/16/2021 3:20:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[WEB_GetFortressInfo]
AS
BEGIN
	SELECT 
	CodeName128 as code,
	REPLACE(CrestPath128, 'ddj', 'png') as icon,
	sf.Introduction as message,
	sf.TaxRatio as tax_ratio,
	sf.Tax as tax,
	g.Name as guild_name
FROM SRO_R_SHARD.dbo._RefSiegeFortress rsf
LEFT JOIN SRO_R_SHARD.dbo._SiegeFortress sf ON rsf.FortressID = sf.FortressID
LEFT JOIN SRO_R_SHARD.dbo._Guild g on sf.GuildID = g.ID
WHERE 
	rsf.Service = 1
ORDER BY 
	rsf.FortressID DESC
END