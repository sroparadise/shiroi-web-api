USE [SRO_R_ACCOUNT]
GO
/****** Object:  StoredProcedure [dbo].[WEB_GetPlayerRanking]    Script Date: 10/27/2021 15:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[WEB_GetPlayerRanking]
@count INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT TOP(@count) 
		CharName16 as Name,
		CASE o.Country WHEN 1 THEN 'EU' ELSE 'CH' END AS Race,
		HwanLevel,
		CurLevel as Level,
		HP, 
		MP,
		RemainGold as Gold,
		RemainSkillPoint as 'SP',
		(
			SELECT COUNT(*) 
			FROM 
				SRO_R_SHARD.dbo._CharQuest cq
				LEFT JOIN SRO_R_SHARD.dbo._RefQuest rq ON rq.ID = cq.QuestID
			WHERE 
				CharID = c.CharID 
				AND Status = 4 
				AND rq.CodeName NOT LIKE 'QNQ_LV%' 
				AND rq.CodeName NOT LIKE 'QTUTORIAL%' 
				AND rq.CodeName NOT LIKE 'QEV%'
				AND rq.CodeName NOT LIKE '%TUTORIAL%'
				AND rq.CodeName NOT LIKE 'QNO_LV%' 
		) as QuestPoints
	FROM SRO_R_SHARD.dbo._Char c
	LEFT JOIN _RefObjCommon o ON o.ID = RefObjID AND o.ID != 0
	--LEFT JOIN SRO_R_SHARD.dbo._User su on su.CharID = c.CharID
	--LEFT JOIN TB_User u on su.CharID = u.JID
	
	WHERE c.CharID NOT IN (
		SELECT CharID FROM SRO_R_SHARD.dbo._User WHERE UserJID IN (
			SELECT JID FROM TB_User WHERE sec_primary IN (1, 2)
		)
	) 

	AND c.CharID NOT IN (
		SELECT CharID FROM SRO_R_SHARD.dbo._User WHERE UserJID IN (
			SELECT UserJID FROM _Punishment WHERE BlockEndTime >= CURRENT_TIMESTAMP
		)
	)

	AND c.CharID > 0 

	AND cast(c.LastLogout as DATE) >= dateadd(dd,-3, cast(CURRENT_TIMESTAMP as DATE))  -- 3 days ago only

	ORDER BY  QuestPoints DESC, CurLevel DESC, COALESCE(HwanLevel, 0) DESC,ExpOffset DESC, HP DESC, MP DESC, RemainGold DESC, RemainSKillPoint DESC
END