USE [SRO_R_ACCOUNT]

GO
DROP TABLE dbo.WEB_Payments
GO

/****** Object:  Table [dbo].[WEB_Payments]    Script Date: 2/18/2021 12:02:39 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[WEB_Payments](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[UserJID] [bigint] NOT NULL,
	[provider] [varchar](50) NOT NULL,
	[payment_ref_init] [varchar](256) NULL,
	[payment_ref_complete] [varchar](256) NULL,
	[payment_amount] [decimal](18, 2) NOT NULL,
	[reward] [bigint] NOT NULL,
	[created_at] [datetime] NOT NULL,
	[updated_at] [datetime] NOT NULL,
 CONSTRAINT [PK_WEB_Payments] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[WEB_Payments] ADD  CONSTRAINT [DF_WEB_Payments_created_at]  DEFAULT (getdate()) FOR [created_at]
GO

ALTER TABLE [dbo].[WEB_Payments] ADD  CONSTRAINT [DF_WEB_Payments_updated_at]  DEFAULT (getdate()) FOR [updated_at]
GO


