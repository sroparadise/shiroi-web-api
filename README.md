# SHIROI.ONLINE front-end-api

Engines:
- [Babel](https://babeljs.io/)
- [KnexJS](https://knexjs.org/)

## How to use
Install it

```
npm install 
# or
yarn install
```

Run it
```
npm run dev
# or
yarn run dev
```


